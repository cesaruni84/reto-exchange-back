/**
 * 
 */
package com.api.exchange.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.api.exchange.entity.Currency;


/**
 * @author CESAR
 *
 */
@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Integer>{

	

	@Query("SELECT c FROM Currency c WHERE c.code = :codeCurrency")
	Optional<Currency> findByCode(@Param("codeCurrency") String codeCurrency);
}
