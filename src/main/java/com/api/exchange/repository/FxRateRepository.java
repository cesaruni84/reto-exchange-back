/**
 * 
 */
package com.api.exchange.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.api.exchange.entity.FxRate;


/**
 * @author CESAR
 *
 */
@Repository
public interface FxRateRepository extends JpaRepository<FxRate, Integer>{

	@Query("SELECT t FROM FxRate t "
			+ "WHERE t.baseCurrency.id = :idBaseCurrency AND t.counterCurrency.id = :idCounterCurrency ")
	Optional<FxRate> findRateByCodeCurrency(@Param("idBaseCurrency") Integer idBaseCurrency, 
			@Param("idCounterCurrency") Integer idCounterCurrency);
}
