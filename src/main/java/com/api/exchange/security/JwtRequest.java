/**
 * 
 */
package com.api.exchange.security;

import java.io.Serializable;

/**
 * @author CESAR
 *
 */
public class JwtRequest implements Serializable{
	
	
	private static final long serialVersionUID = -977372417109082531L;
	private String username;
	private String password;
	
	
	public JwtRequest() {
	}
	public JwtRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
