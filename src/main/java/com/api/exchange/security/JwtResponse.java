/**
 * 
 */
package com.api.exchange.security;

import java.io.Serializable;

/**
 * @author CESAR
 *
 */
public class JwtResponse implements Serializable {
	
	private static final long serialVersionUID = 4206350592914918227L;
	private final String jwttoken;

	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}

}
