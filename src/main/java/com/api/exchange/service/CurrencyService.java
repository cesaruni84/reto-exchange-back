/**
 * 
 */
package com.api.exchange.service;

import java.util.List;
import com.api.exchange.entity.Currency;
import io.reactivex.Single;



/**
 * @author CESAR
 *
 */
public interface CurrencyService {
	
	Single<Currency> save(Currency currency);
	
    Single<List<Currency>> findAll();
     
    Single<Currency> findByCode(String code);
 

 

}
