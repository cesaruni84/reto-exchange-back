/**
 * 
 */
package com.api.exchange.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.exchange.entity.Currency;
import com.api.exchange.repository.CurrencyRepository;
import com.api.exchange.service.CurrencyService;
import io.reactivex.Single;


/**
 * @author CESAR
 *
 */
@Service
public class CurrencyServiceImpl implements CurrencyService {
	
	@Autowired
	private CurrencyRepository repository;

	@Override
	public Single<Currency> save(Currency currency) {
        return Single.create(singleSubscriber -> {
            singleSubscriber.onSuccess(repository.save(currency));
        });
	}

	@Override
	public Single<List<Currency>> findAll() {
        return Single.create(singleSubscriber -> {
            List<Currency> listCurrency = repository.findAll();
            singleSubscriber.onSuccess(listCurrency);
        });
	}

	@Override
	public Single<Currency> findByCode(String code) {
        return Single.create(singleSubscriber -> {
        	Optional<Currency> objCurrency = repository.findByCode(code);
        	if (objCurrency.isPresent()) {
                singleSubscriber.onSuccess(objCurrency.get());
        	} else {
               singleSubscriber.onError(new Throwable());
        	}
        });
	}






}
