/**
 * 
 */
package com.api.exchange.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.exchange.dto.QueryExchangeRequestDTO;
import com.api.exchange.dto.QueryExchangeResponseDTO;
import com.api.exchange.entity.Currency;
import com.api.exchange.entity.FxRate;
import com.api.exchange.repository.CurrencyRepository;
import com.api.exchange.repository.FxRateRepository;
import com.api.exchange.service.FxRateService;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author CESAR
 *
 */
@Service
public class FxRateServiceImpl implements FxRateService {
	
	@Autowired
	private FxRateRepository repository;
	
	@Autowired
	private CurrencyRepository currencyRepository;
	
	
	@Override
	public Single<FxRate> save(FxRate fxRate) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Completable update(FxRate fxRateUpdate) {
		return Completable.create(completableSubscriber -> {
			Optional<FxRate> optionalFxRate = repository.findById(fxRateUpdate.getId());
			if (!optionalFxRate.isPresent())
				completableSubscriber.onError(new EntityNotFoundException());
			else {
				FxRate fxRate_ = optionalFxRate.get();
				if (fxRateUpdate.getRate().compareTo(BigDecimal.ZERO) > 0) {
					fxRate_.setRate(fxRateUpdate.getRate());
					//fxRate_.setInverseRate(new BigDecimal(1.00).divide(fxRateUpdate.getRate()));
					Timestamp ts = new Timestamp(System.currentTimeMillis());
					fxRate_.setDate(new Date(ts.getTime()));
				}
				repository.save(fxRate_);
				completableSubscriber.onComplete();
			}
		});
	}
	

	@Override
	public Single<QueryExchangeResponseDTO> findRateByCodeCurrency(QueryExchangeRequestDTO queryExchangeRequestDTO ) {
		
        return Single.create(singleSubscriber -> {
    		Optional<Currency> currencyBase  = currencyRepository.findByCode(queryExchangeRequestDTO.getSourceCurrency());
    		Optional<Currency> currencyEval  = currencyRepository.findByCode(queryExchangeRequestDTO.getDestinationCurrency());
    		QueryExchangeResponseDTO queryExchangeResponseDTO = new QueryExchangeResponseDTO();
    		
    		if (currencyBase.isPresent() && currencyEval.isPresent()) {
    			Optional<FxRate> rate =	repository.findRateByCodeCurrency(currencyBase.get().getId(), currencyEval.get().getId());
    			if (rate.isPresent()) {
    				queryExchangeResponseDTO.setIdTrx(UUID.randomUUID().toString());
    				queryExchangeResponseDTO.setDestinationAmount(rate.get().getRate().multiply(queryExchangeRequestDTO.getSourceAmount()));
    				queryExchangeResponseDTO.setDestinationCurrency(queryExchangeRequestDTO.getDestinationCurrency());
    				queryExchangeResponseDTO.setRate(rate.get().getRate());
    				queryExchangeResponseDTO.setSourceAmount(queryExchangeRequestDTO.getSourceAmount());
    				queryExchangeResponseDTO.setSourceCurrency(queryExchangeRequestDTO.getSourceCurrency());
    		        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    				queryExchangeResponseDTO.setTimestamp(timestamp.toString());
    				queryExchangeResponseDTO.setIdTrx(UUID.randomUUID().toString());
    	            singleSubscriber.onSuccess(queryExchangeResponseDTO);
    			} else {
    	            singleSubscriber.onError(new Throwable());
    			}
    		}
        });
		
	}


	@Override
	public Single<List<FxRate>> findAll() {
        return Single.create(singleSubscriber -> {
            List<FxRate> listRates = repository.findAll();
            singleSubscriber.onSuccess(listRates);
        });
	}





	



}
