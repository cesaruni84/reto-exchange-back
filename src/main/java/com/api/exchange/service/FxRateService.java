package com.api.exchange.service;

import java.util.List;
import com.api.exchange.dto.QueryExchangeRequestDTO;
import com.api.exchange.dto.QueryExchangeResponseDTO;
import com.api.exchange.entity.FxRate;
import io.reactivex.Completable;
import io.reactivex.Single;


public interface FxRateService {
	
	Single<FxRate> save(FxRate fxRate);
    
	Completable update(FxRate fxRate);
	
    Single<List<FxRate>> findAll();

	Single<QueryExchangeResponseDTO> findRateByCodeCurrency(QueryExchangeRequestDTO queryExchangeRequestDTO ); 

}
