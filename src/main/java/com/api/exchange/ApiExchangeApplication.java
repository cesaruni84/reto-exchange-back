package com.api.exchange;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.api.exchange.repository.*;
import com.api.exchange.entity.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class ApiExchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiExchangeApplication.class, args);
	}
	
	@Bean
	CommandLineRunner runner(CurrencyRepository currencyRepo, FxRateRepository rateRepo){
	    return args -> {
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Currency>> typeReference1 = new TypeReference<List<Currency>>(){};
			TypeReference<List<FxRate>> typeReference2 = new TypeReference<List<FxRate>>(){};
			InputStream inputStream1 = TypeReference.class.getResourceAsStream("/monedas.json");
			InputStream inputStream2 = TypeReference.class.getResourceAsStream("/rates.json");

			try {
				List<Currency> listCurrencyInitial = mapper.readValue(inputStream1,typeReference1);
				currencyRepo.saveAll(listCurrencyInitial);
				mapper = new ObjectMapper();
				List<FxRate> listRatesInitial = mapper.readValue(inputStream2,typeReference2);
				rateRepo.saveAll(listRatesInitial);
			} catch (IOException e){
				System.out.println("No ha sido posible cargar archivo rates.json " + e.getMessage());
			}
	    };
	}

}
