/**
 * 
 */
package com.api.exchange.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author CESAR
 *
 */
public class QueryExchangeResponseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("id_transaction")
	private String idTrx;
	
	@JsonProperty("source_amount")
	private BigDecimal sourceAmount;
	
	@JsonProperty("source_currency")
	private String sourceCurrency;
	
	@JsonProperty("destination_currency")
	private String destinationCurrency;
	
	@JsonProperty("destination_amount")
	private BigDecimal destinationAmount;
	
	@JsonProperty("rate")
	private BigDecimal rate;
	
	@JsonProperty("timestamp")
	private String timestamp;
	
	
	public String getIdTrx() {
		return idTrx;
	}
	public void setIdTrx(String idTrx) {
		this.idTrx = idTrx;
	}
	public BigDecimal getSourceAmount() {
		return sourceAmount;
	}
	public void setSourceAmount(BigDecimal sourceAmount) {
		this.sourceAmount = sourceAmount;
	}
	public String getSourceCurrency() {
		return sourceCurrency;
	}
	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}
	public String getDestinationCurrency() {
		return destinationCurrency;
	}
	public void setDestinationCurrency(String destinationCurrency) {
		this.destinationCurrency = destinationCurrency;
	}
	public BigDecimal getDestinationAmount() {
		return destinationAmount;
	}
	public void setDestinationAmount(BigDecimal destinationAmount) {
		this.destinationAmount = destinationAmount;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


}
