/**
 * 
 */
package com.api.exchange.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.api.exchange.dto.MessageResponse;
import com.api.exchange.dto.QueryExchangeRequestDTO;
import com.api.exchange.dto.QueryExchangeResponseDTO;
import com.api.exchange.entity.FxRate;
import com.api.exchange.service.FxRateService;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;


/**
 * @author CESAR
 *
 */

@RestController
@RequestMapping(value = "/api/exchange")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FxRateController {

	
	@Autowired
	private FxRateService fxRateService;
	
	@GetMapping(value = "/rates")
	public Single<ResponseEntity<List<FxRate>>> listAllRates(){
		// return new ResponseEntity<Flux<FxRate>>(listRates, HttpStatus.OK);
        return fxRateService.findAll()
                .subscribeOn(Schedulers.io())
                .map(listRates -> ResponseEntity.ok(listRates));	
    }

	@PostMapping(value = "/rates" , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	public Single<ResponseEntity<QueryExchangeResponseDTO>> QueryExchangeMoneyReq(@Valid @RequestBody QueryExchangeRequestDTO queryExchangeRequestDTO){
		
        return fxRateService.findRateByCodeCurrency(queryExchangeRequestDTO)
                .subscribeOn(Schedulers.io())
                .map(response -> ResponseEntity.ok(response));
	}
	
	@PutMapping(value = "/rates" , consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	public Single<ResponseEntity<MessageResponse>> updateExchangeMoney(@Valid @RequestBody FxRate fxRateUpdated){
		
        return fxRateService.update(fxRateUpdated)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(new MessageResponse("MPA000","Tipo de Cambio actualizado correctamente")));
        
	}
	
}
