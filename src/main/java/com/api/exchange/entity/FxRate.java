package com.api.exchange.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_rate")
public class FxRate {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "id_base_currency", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "fk_currency_base"))
	private Currency baseCurrency;
	
	@ManyToOne
	@JoinColumn(name = "id_counter_currency", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "fk_currency_counter"))
	private Currency counterCurrency;
	
	private BigDecimal rate;
	private BigDecimal inverseRate;
	private Date date;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Currency getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(Currency baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public Currency getCounterCurrency() {
		return counterCurrency;
	}
	public void setCounterCurrency(Currency counterCurrency) {
		this.counterCurrency = counterCurrency;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public BigDecimal getInverseRate() {
		return inverseRate;
	}
	public void setInverseRate(BigDecimal inverseRate) {
		this.inverseRate = inverseRate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
	

}
