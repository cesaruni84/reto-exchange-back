FROM openjdk:8
MAINTAINER cesaruni cesaruni84@gmail.com
EXPOSE 8080
ADD target/api-exchange-docker.jar api-exchange-docker.jar
ENTRYPOINT ["java", "-jar", "/api-exchange-docker.jar"]